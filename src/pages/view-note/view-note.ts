import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {NoteService} from '../../providers/note-service/note-service';
import {Note} from "../../models/note.model";
import { ActionSheetController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-view-note',
  templateUrl: 'view-note.html',
})
export class ViewNotePage {
  note: Note;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private noteService: NoteService,
              public actionSheetCtrl: ActionSheetController,
              public platform: Platform,) {
                this.note = this.navParams.get('note');
  }


  openActionSheet(createDate: number){

    console.log('opening');
    let actionsheet = this.actionSheetCtrl.create({
      title:"Delete Notes?",
      buttons:[{
        text: 'Yes',
        role: 'destructive',
        icon: !this.platform.is('ios') ? 'trash' : null,

        handler: () => {
          this.noteService.deleteNote(createDate);
          this.navCtrl.pop();
        }
      },{
        text: 'No',
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => {
        }
      }]
    });
    actionsheet.present();
  }

}
